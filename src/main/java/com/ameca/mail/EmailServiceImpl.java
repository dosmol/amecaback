package com.ameca.mail;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl implements EmailService {
 
    @Autowired
    private JavaMailSender emailSender;
 
    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
    	ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
        emailExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                	SimpleMailMessage message = new SimpleMailMessage(); 
                    message.setFrom("noreply@baeldung.com");
                    message.setTo(to); 
                    message.setSubject(subject); 
                    message.setText(text);
                    emailSender.send(message);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        });
        emailExecutor.shutdown(); 
        
       
    }
}