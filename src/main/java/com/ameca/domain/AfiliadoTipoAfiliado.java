package com.ameca.domain;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class AfiliadoTipoAfiliado {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "afiTipoAfi_seq")
	private long id;
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Afiliado afiliado;
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private TipoAfiliado tipoFiliado;
	@Column
	@CreationTimestamp
	private LocalDateTime fechaAlta;
	@Column
	private LocalDateTime fechaBaja;
	
	
	public AfiliadoTipoAfiliado() {
		super();
	}
	public AfiliadoTipoAfiliado(Afiliado afiliado, TipoAfiliado tipoFiliado, LocalDateTime date) {
		super();
		this.afiliado = afiliado;
		this.tipoFiliado = tipoFiliado;
		this.fechaAlta = date;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Afiliado getAfiliado() {
		return afiliado;
	}
	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}
	public TipoAfiliado getTipoFiliado() {
		return tipoFiliado;
	}
	public void setTipoFiliado(TipoAfiliado tipoFiliado) {
		this.tipoFiliado = tipoFiliado;
	}
	public LocalDateTime getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDateTime fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public LocalDateTime getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(LocalDateTime fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	
	
	
}
