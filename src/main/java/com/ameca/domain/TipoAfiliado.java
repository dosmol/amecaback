package com.ameca.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class TipoAfiliado implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipoAfi_seq")
	private long id;
	@Column
	private String nombre;
	@Column
	private Integer orden;
	@Column
	private Boolean activo=true;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;
	
	
	public TipoAfiliado(String nombre, Boolean activo,Integer orden) {
		super();
		this.nombre = nombre;
		this.activo = activo;
		this.orden = orden;
	}
	public TipoAfiliado() {
		super();
		
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public LocalDateTime getTs() {
		return ts;
	}
	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}
	public LocalDateTime getTu() {
		return tu;
	}
	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}
	
}
