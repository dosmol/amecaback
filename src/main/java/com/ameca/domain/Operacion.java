package com.ameca.domain;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Operacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operacion_seq")
	private long id;
	@Column
	private String nombre;
	@Column
	private String descripcion;
	@Column
	private Integer orden;
	@Column
	private String urlComponent;
	@Column
	private Boolean activo=true;
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private TipoOperacion tipo;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "OPERACION_ROL", joinColumns = {
			@JoinColumn(name = "OPERACION_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLE_ID") })
	private Set<Role> roles;
	
	
	public Operacion() {
		super();
	}
	public Operacion( String nombre, String descripcion, Boolean activo,TipoOperacion tipo,Integer orden) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.activo = activo;
		this.tipo = tipo;
		this.orden = orden;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
	
	public TipoOperacion getTipo() {
		return tipo;
	}
	public void setTipo(TipoOperacion tipo) {
		this.tipo = tipo;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public LocalDateTime getTs() {
		return ts;
	}
	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}
	public LocalDateTime getTu() {
		return tu;
	}
	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}
	public String getUrlComponent() {
		return urlComponent;
	}
	public void setUrlComponent(String urlComponent) {
		this.urlComponent = urlComponent;
	}
	
	

}
