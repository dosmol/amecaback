package com.ameca.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.ameca.web.presentation.AuthorizationRequest;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@Data
public class User implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name = "user_generator", initialValue = 100)
	private long id;
	@Column
	private String name;
	@Column
	private String email;
	@Column
	private String password;
	@Column
	private Boolean isPassReseted=false;;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "USER_ROLES", joinColumns = {
			@JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLE_ID") })
	private Set<Role> roles; 
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "USER_FILIAL", 
			joinColumns = {	@JoinColumn(name = "USER_ID") }, 
			inverseJoinColumns = {@JoinColumn(name = "FILIAL_ID") })
	private Set<Filial> filiales;
	@Column
	private LocalDateTime fechaEliminacion;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;

	public User(String name, String email) {
		super();
		this.name = name;
		this.email = email;
		this.filiales = new HashSet<Filial>();
		this.roles = new HashSet<Role>();
	}

	public User(String name, String password, Set<Role> roles) {
		super();
		this.name = name;
		this.password = password;
		this.roles = roles;
	}

	public long getId() {
		return id;
	}

	public Set<Filial> getFiliales() {
		return filiales;
	}

	public void setFiliales(Set<Filial> filiales) {
		this.filiales = filiales;
	}

	public User() {
		super();
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsPassReseted() {
		return isPassReseted;
	}

	public void setIsPassReseted(Boolean isPassReseted) {
		this.isPassReseted = isPassReseted;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}

	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	public LocalDateTime getTs() {
		return ts;
	}

	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}

	public LocalDateTime getTu() {
		return tu;
	}

	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}
	
	public Boolean isAdmin() {
		for (Role rol : this.roles) {
			if(rol.getName().equals("ADMIN")) {
				return true;
			}
		}
		return false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void actualizar(AuthorizationRequest usuario) {
		this.name = usuario.getUser();
		this.email = usuario.getEmail();
	}


	private static final long serialVersionUID = 1L;

	
}
