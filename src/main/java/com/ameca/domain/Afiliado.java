package com.ameca.domain;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.ameca.web.presentation.AfiliadoRequest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Afiliado {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "afiliado_seq")
	private long id;
	@Column
	private String nombre;
	@Column
	private String apellido;
	@Column
	private String direccion;
	@Column
	private String telefono;
	@Column
	private String celular;
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private TipoDocumento tipoDoc;
	@Column
	private String numDoc;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	private LocalDateTime fechaEliminacion;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;
	@OneToMany(mappedBy="afiliado")
    private Set<AfiliadoTipoAfiliado> tiposAfiliado;
	@OneToMany(mappedBy="afiliado")
    private Set<AfiliadoFilial> filiales;
	
	
	
	public Afiliado(long id, String nombre, String apellido, String direccion, String telefono, String celular,
			TipoDocumento tipoDoc, String numDoc, LocalDateTime ts, LocalDateTime fechaEliminacion, LocalDateTime tu) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.tipoDoc = tipoDoc;
		this.numDoc = numDoc;
		this.ts = ts;
		this.fechaEliminacion = fechaEliminacion;
		this.tu = tu;
	}
	public Afiliado(String nombre, String apellido, String direccion, String telefono, String celular, String numDoc) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.numDoc = numDoc;
	}
	public Afiliado() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public TipoDocumento getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(TipoDocumento tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNumDoc() {
		return numDoc;
	}
	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}
	public LocalDateTime getTs() {
		return ts;
	}
	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}
	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}
	public LocalDateTime getTu() {
		return tu;
	}
	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}
	public Set<AfiliadoTipoAfiliado> getTiposAfiliado() {
		return tiposAfiliado;
	}
	public void setTiposAfiliado(Set<AfiliadoTipoAfiliado> tiposAfiliado) {
		this.tiposAfiliado = tiposAfiliado;
	}
	public TipoAfiliado getTipoActual() {
		for (AfiliadoTipoAfiliado afiliadoTipoAfiliado : this.tiposAfiliado) {
			if(afiliadoTipoAfiliado.getFechaBaja() == null) {
				return afiliadoTipoAfiliado.getTipoFiliado();
			}
		}
		return new TipoAfiliado();
	}
	public void bajaTipoAfiliado(LocalDateTime fechaBaja) {
		for (AfiliadoTipoAfiliado afiliadoTipoAfiliado : this.tiposAfiliado) {
			if(afiliadoTipoAfiliado.getFechaBaja() == null) {
				afiliadoTipoAfiliado.setFechaBaja(fechaBaja);
			}
		}
	}
	public Set<AfiliadoFilial> getFilialesAfiliado() {
		return this.filiales;
	}
	public void setFilialesAfiliado(Set<AfiliadoFilial> filiales) {
		this.filiales = filiales;
	}
	public Filial getFilialActual() {
		for (AfiliadoFilial filial : this.filiales) {
			if(filial.getFechaBaja() == null) {
				return filial.getTipoFiliado();
			}
		}
		return new Filial();
	}
	public void bajaFilial(LocalDateTime fechaBaja) {
		for (AfiliadoFilial filial : this.filiales) {
			if(filial.getFechaBaja() == null) {
				filial.setFechaBaja(fechaBaja);
			}
		}
	}
	
	public void actualizar(AfiliadoRequest afiReq) {
		this.nombre = afiReq.getNombre();
		this.apellido = afiReq.getApellido();
		this.direccion = afiReq.getDireccion();
		this.telefono = afiReq.getTelefono();
		this.celular = afiReq.getCelular();
		this.numDoc = afiReq.getNumDoc();
	}
	
	

}
