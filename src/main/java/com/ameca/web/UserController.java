package com.ameca.web;

import com.ameca.domain.Filial;
import com.ameca.domain.Role;
import com.ameca.domain.User;
import com.ameca.domain.list.OperacionList;
import com.ameca.domain.list.UserList;
import com.ameca.mail.EmailService;
import com.ameca.mapper.ConfiguracionMapper;
import com.ameca.mapper.UserMapper;
import com.ameca.service.ConfiguracionService;
import com.ameca.service.UserService;
import com.ameca.service.UtilsService;
import com.ameca.utils.TokenProvider;
import com.ameca.web.presentation.ActualizarClaveRequest;
import com.ameca.web.presentation.ActualizarClaveResponse;
import com.ameca.web.presentation.AuthorizationRequest;
import com.ameca.web.presentation.TipoOperacionResponse;
import com.ameca.web.presentation.UserResponse;
import com.ameca.web.presentation.list.TipoOperacionResponoseList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController extends BaseController{

	@Autowired
	private UserService userService;
	@Autowired
	private ConfiguracionService configuracionService;
	@Autowired
	private UtilsService utilsService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/getCurrentUser")
	public ResponseEntity<UserResponse> getCurrentUser() {
		Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		org.springframework.security.core.userdetails.User usuario = (org.springframework.security.core.userdetails.User)user;
		String nombre = usuario.getUsername();
		
		User usu = this.userService.getUser(nombre);

		UserResponse userResponse = UserMapper.toResponse(usu);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<UserResponse> getUser(@PathVariable long id) {
		final User user = userService.getUser(id);

		if (user == null) {
			return ResponseEntity.notFound().build();
		}

		UserResponse userResponse = UserMapper.toResponse(user);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<ArrayList<UserResponse>> getUsers() {
		final UserList users = userService.getUsers();

		if (users == null) {
			return ResponseEntity.notFound().build();
		}

		ArrayList<UserResponse> userResponse = UserMapper.toResponse(users);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/getOperacionesHabilitadas")
	public ResponseEntity<ArrayList<TipoOperacionResponse>> getOperaciones() {
		Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		org.springframework.security.core.userdetails.User usuario = (org.springframework.security.core.userdetails.User)user;
		String nombre = usuario.getUsername();
		
		HashMap<String, OperacionList> tipos  = configuracionService.getTiposOperaciones(nombre);

		TipoOperacionResponoseList tiposResponse = ConfiguracionMapper.toResponse(tipos);
		return new ResponseEntity<>(tiposResponse, HttpStatus.OK);
	}

	
	//Métodos post
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping
	public ResponseEntity<UserResponse> saveUser(@RequestBody AuthorizationRequest userRequest) {
		Optional<Role> rol = this.utilsService.getRol(userRequest.getRol());
		Optional<Filial> filial = this.configuracionService.getFilial(userRequest.getFilial());
		User user;
		String clave="";
		if(userRequest.getId()!=null) {
			user = this.userService.getUser(userRequest.getId());
			user.actualizar(userRequest);
			user.getFiliales().clear();
			user.getRoles().clear();
		}else {
			user = UserMapper.toDomain(userRequest);
			clave =TokenProvider.generarContraTemporal(6);
			user.setPassword(bCryptPasswordEncoder.encode(clave));
		}
		user.getFiliales().add(filial.get());
		user.getRoles().add(rol.get());
		User userToSave = userService.save(user);
		if(userRequest.getId()==null) {
			this.emailService.sendSimpleMessage(user.getEmail(), "Creación usuario", "Estimado usuario "+user.getName()+".\n"
					+" Se ha creado un nuevo usuario para el sistema de gestión de AMECA. Su clave es "+clave
					+" En el prmier ingreso se le solicitará que por cuestiones de seguridad la misma sea cambiada.\n"
					+" Muchas gracias.");
		}
		//userRequest.setPassword();
		return new ResponseEntity<UserResponse>(UserMapper.toResponse(userToSave), HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/cambiarEstado")
	public ResponseEntity<UserResponse> cambiarEstado(@RequestBody AuthorizationRequest userRequest) {
		if(userRequest.getId()!=null) {
			User user = this.userService.getUser(userRequest.getId());
			user.setFechaEliminacion(userRequest.getFechaEliminacion());
			return new ResponseEntity<UserResponse>(UserMapper.toResponse(this.userService.save(user)), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@PostMapping("/actualizarPassword")
	public ResponseEntity<ActualizarClaveResponse> actualizarPassword(@RequestBody ActualizarClaveRequest clavesRequest) {
		Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		
	
		org.springframework.security.core.userdetails.User usuario = (org.springframework.security.core.userdetails.User)user;
		User usu = this.userService.getUser(usuario.getUsername());
		ActualizarClaveResponse response = new ActualizarClaveResponse();
		if(bCryptPasswordEncoder.matches(clavesRequest.getClaveActual(), usu.getPassword())){
			if(clavesRequest.getClaveNueva().equals(clavesRequest.getRepeClaveNueva())){
				usu.setPassword(bCryptPasswordEncoder.encode(clavesRequest.getClaveNueva()));
				usu.setIsPassReseted(true);
				if(this.userService.save(usu)!=null) {
					response.setMsg("Contraseña actualizada correctamente.");
					return new ResponseEntity<ActualizarClaveResponse>(response,HttpStatus.OK);
				}else {
					response.setMsg("Error al actualizar la clave. Intente en unos mintuos.");
					return new ResponseEntity<ActualizarClaveResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
				}
				
			}else {
				response.setMsg("Las claves ingresadas con coniciden.");
				return new ResponseEntity<ActualizarClaveResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			response.setMsg("La clave actual que se ingresó, es incorrecta.");
			return new ResponseEntity<ActualizarClaveResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		
		/*if(userRequest.getId()!=null) {
			User user = this.userService.getUser(userRequest.getId());
			user.setFechaEliminacion(userRequest.getFechaEliminacion());
			return new ResponseEntity<UserResponse>(UserMapper.toResponse(this.userService.save(user)), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}*/

		
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/resetPassword")
	public ResponseEntity<Object> resetPassword(@RequestBody AuthorizationRequest userRequest) {
		if(userRequest.getId()!=null) {
			User user = this.userService.getUser(userRequest.getId());
			String clave =TokenProvider.generarContraTemporal(6);
			user.setPassword(bCryptPasswordEncoder.encode(clave));
			user.setIsPassReseted(false);
			this.userService.save(user);
			this.emailService.sendSimpleMessage(user.getEmail(), "Actualización de clave", "Estimado usuario "+user.getName()+".\n"
					+" Se ha actualizado su clave para el sistema de gestión de AMECA. La nueva clave es "+clave
					+" En el prmier ingreso se le solicitará que por cuestiones de seguridad la misma sea cambiada.\n"
					+" Muchas gracias.");
			return new ResponseEntity<>(null, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
	}
}
