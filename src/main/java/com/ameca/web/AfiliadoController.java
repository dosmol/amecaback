package com.ameca.web;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ameca.mapper.AfiliadoMapper;
import com.ameca.service.AfiliadoService;
import com.ameca.service.ConfiguracionService;
import com.ameca.service.UtilsService;
import com.ameca.web.presentation.AfiliadoRequest;
import com.ameca.web.presentation.AfiliadoResponse;
import com.ameca.web.presentation.MensajeGeneralResponse;
import com.ameca.domain.Afiliado;
import com.ameca.domain.AfiliadoFilial;
import com.ameca.domain.AfiliadoTipoAfiliado;
import com.ameca.domain.Filial;
import com.ameca.domain.TipoAfiliado;
import com.ameca.domain.TipoDocumento;
import com.ameca.domain.User;

@RestController
@RequestMapping("/afiliado")
@CrossOrigin
public class AfiliadoController extends BaseController{
	
	//Creo todos los servicios que voy a usar en el controlador
	@Autowired
	private AfiliadoService afiliadoService;
	@Autowired
	private UtilsService utilsService;
	@Autowired
	private ConfiguracionService configuracionService;
	
	
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@PostMapping
	public ResponseEntity<MensajeGeneralResponse> saveAfiliado(@RequestBody AfiliadoRequest afiliadoRequest) {
		
		Optional<TipoDocumento> tipo = utilsService.getTipoDocumento(afiliadoRequest.getTipoDoc());
		Optional<TipoAfiliado> tipoAfiliado = utilsService.getTipoAfiliado(afiliadoRequest.getTipoAfiliado());
		Optional<Filial> filial = configuracionService.getFilial(afiliadoRequest.getFilial());
		
		
		if(afiliadoRequest.getId()==null) {//Cuando el afiliado es nuevo, todo se tiene que crear desde cero
			Afiliado afiliado = AfiliadoMapper.toDomain(afiliadoRequest);
			afiliado.setTipoDoc(tipo.get());
			//Guardo los datos enviados para el Afiliado
			Afiliado afiSaved = this.afiliadoService.save(afiliado);
			//Reviso, si hay cambio de tipo de afiliación. Si hay doy de baja la anterior, y creo una nueva.
			AfiliadoTipoAfiliado afiTipo = new AfiliadoTipoAfiliado(afiSaved,tipoAfiliado.get(),LocalDateTime.now());
			this.utilsService.saveAfiliadoTipoAfiliado(afiTipo);
			AfiliadoFilial afiFilial = new AfiliadoFilial(afiSaved,filial.get(),LocalDateTime.now()); 
			this.configuracionService.save(afiFilial);
			
		}else {//Cuando es una actualización, busco el afiliado que se está queriendo actualizar para cambiar las cosas
			Afiliado afiliadodb =this.afiliadoService.findById(afiliadoRequest.getId()).get();
			afiliadodb.actualizar(afiliadoRequest);
			afiliadodb.setTipoDoc(tipo.get());
			afiliadodb.setFechaEliminacion(null);//Me aseguro que si es una reactivación de usuario, el mismo se active
			if(afiliadodb.getTipoActual().getId()!=afiliadoRequest.getTipoAfiliado()) {
				afiliadodb.bajaTipoAfiliado(LocalDateTime.now());
				AfiliadoTipoAfiliado afiTipo = new AfiliadoTipoAfiliado(afiliadodb,tipoAfiliado.get(),LocalDateTime.now());
				this.utilsService.saveAfiliadoTipoAfiliado(afiTipo);
			}
			if(afiliadodb.getFilialActual().getId() != afiliadoRequest.getFilial()) {
				afiliadodb.bajaFilial(LocalDateTime.now());
				AfiliadoFilial afiFilial = new AfiliadoFilial(afiliadodb,filial.get(),LocalDateTime.now());
				this.configuracionService.save(afiFilial);
				
			}
			this.afiliadoService.save(afiliadodb);
		}
		
		ArrayList<String> mensajes = new ArrayList<>();
		mensajes.add("El afiliado se cargó de manera correcta");
		MensajeGeneralResponse mensaje = new MensajeGeneralResponse("Afiliado Cargado Correctamente",mensajes);
		
		return new ResponseEntity<>(mensaje, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<ArrayList<AfiliadoResponse>> getAfiliadosActivos() {
		User user =getUser();
		ArrayList<Afiliado> afiliados;
		if(user.isAdmin()) {
			afiliados = this.afiliadoService.getAfiliadosActivos();
		}else {
			afiliados = this.afiliadoService.getAfiliadosActivos(user.getFiliales());
		}
		return new ResponseEntity<>(AfiliadoMapper.toResponse(afiliados), HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("verificar")
	public ResponseEntity<AfiliadoResponse> verificarAfiliado(@RequestParam(name="tipoDoc",required=true) Long tipoDoc,
																		 @RequestParam(name="numDoc",required=true) String numDoc) {
		
		
		Optional<Afiliado> afiliado = this.afiliadoService.findBytipoDocAndnumDoc(tipoDoc,numDoc);
		AfiliadoResponse afiRes = new AfiliadoResponse();
		if(afiliado.isPresent()) {
			afiRes = AfiliadoMapper.toResponse(afiliado.get());
		}
		
		return new ResponseEntity<AfiliadoResponse>(afiRes, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("{id}")
	public ResponseEntity<AfiliadoResponse> getAfiliadoActivo(@PathVariable Long id) {

		Optional<Afiliado>  afiliado = this.afiliadoService.findById(id);
		
		return new ResponseEntity<AfiliadoResponse>(AfiliadoMapper.toResponse(afiliado.get()), HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@DeleteMapping("{id}")
	public ResponseEntity<AfiliadoResponse> deleteAfiliado(@PathVariable Long id) {

		if(this.afiliadoService.delete(id)) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		
	}
	
}
