package com.ameca.web.presentation;


import com.ameca.domain.TipoDocumento;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TipoDocumentoResponse {
	private long id;
	private String nombre;
	private Boolean activo;
	
	public TipoDocumentoResponse(long id, String nombre, Integer orden) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public TipoDocumentoResponse(TipoDocumento tipoDocumento) {
		super();
		this.id = tipoDocumento.getId();
		this.nombre = tipoDocumento.getNombre();
		this.activo = tipoDocumento.getActivo();
	}
	public TipoDocumentoResponse() {
		super();
		
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
}
