package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CiudadResponse {
	private long id;
	private String nombre;
	private Boolean activo;
	public CiudadResponse() {
		super();
	}
	public CiudadResponse(long id, String nombre, Boolean activo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activo = activo;
	}
	public CiudadResponse(String nombre, Boolean activo) {
		super();
		this.nombre = nombre;
		this.activo = activo;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
}