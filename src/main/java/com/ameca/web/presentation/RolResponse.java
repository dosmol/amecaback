package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.io.Serializable;
import com.ameca.domain.Role;

@Data
@AllArgsConstructor
public class RolResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String nombre,descripcion;
	
	
	public RolResponse() {
		super();
	}

	public RolResponse(long id, String nombre, String descripcion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public RolResponse(Role rol) {
		super();
		if(rol!=null) {
			this.id = rol.getId();
			this.nombre = rol.getName();
			this.descripcion = rol.getDescription();
		}
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
