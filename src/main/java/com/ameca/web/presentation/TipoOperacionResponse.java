package com.ameca.web.presentation;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TipoOperacionResponse {
	private long id;
	private String nombre;
	private Integer orden;
	private List<OperacionResponse> operaciones;
	
	public TipoOperacionResponse(long id, String nombre, Integer orden,List<OperacionResponse> operaciones) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.orden = orden;
		this.operaciones = operaciones;
	}
	public TipoOperacionResponse(String nombre,List<OperacionResponse> operaciones) {
		super();
		this.nombre = nombre;
		this.operaciones = operaciones;
	}
	public TipoOperacionResponse() {
		super();
		
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public List<OperacionResponse> getOperaciones() {
		return operaciones;
	}
	public void setOperaciones(List<OperacionResponse> operaciones) {
		this.operaciones = operaciones;
	}
	
	

}
