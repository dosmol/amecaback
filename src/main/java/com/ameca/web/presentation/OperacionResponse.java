package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OperacionResponse {
	private long id;
	private String nombre,descripcion;
	private Integer orden;
	private Boolean activo;
	private TipoOperacionResponse tipo;
	private String urlComponent;
	public OperacionResponse(long id, String nombre, String descripcion, Integer orden, Boolean activo,String urlComponent, TipoOperacionResponse tipo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.orden = orden;
		this.activo = activo;
		this.tipo = tipo;
		this.urlComponent = urlComponent;
	}
	public OperacionResponse(long id, String nombre, String descripcion, Integer orden, Boolean activo,String urlComponent) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.orden = orden;
		this.activo = activo;
		this.urlComponent = urlComponent;
	}
	public OperacionResponse() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public TipoOperacionResponse getTipo() {
		return tipo;
	}
	public void setTipo(TipoOperacionResponse tipo) {
		this.tipo = tipo;
	}
	public String getUrlComponent() {
		return urlComponent;
	}
	public void setUrlComponent(String urlComponent) {
		this.urlComponent = urlComponent;
	}
	
	

}
