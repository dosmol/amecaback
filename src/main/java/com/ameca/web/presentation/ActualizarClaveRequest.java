package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class ActualizarClaveRequest {
	
	private String claveActual,claveNueva,repeClaveNueva;
	
	

	public ActualizarClaveRequest() {
		super();
	}

	public ActualizarClaveRequest(String actual, String nueva, String reNueva) {
		super();
		this.claveActual = actual;
		this.claveNueva = nueva;
		this.repeClaveNueva = reNueva;
	}

	
	
	public String getClaveActual() {
		return claveActual;
	}

	public void setClaveActual(String claveActual) {
		this.claveActual = claveActual;
	}

	public String getClaveNueva() {
		return claveNueva;
	}

	public void setClaveNueva(String claveNueva) {
		this.claveNueva = claveNueva;
	}

	public String getRepeClaveNueva() {
		return repeClaveNueva;
	}

	public void setRepeClaveNueva(String repeClaveNueva) {
		this.repeClaveNueva = repeClaveNueva;
	}

	public Boolean isCompletamenteVacio() {
		return this.claveActual.isEmpty()||this.claveNueva.isEmpty()||this.repeClaveNueva.isEmpty();
	}
	
	
	
}