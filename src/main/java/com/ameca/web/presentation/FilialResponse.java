package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

import com.ameca.domain.Filial;
import com.ameca.mapper.ConfiguracionMapper;

@Data
@AllArgsConstructor
public class FilialResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String nombre,direccion;
	private CiudadResponse ciudad;
	private Boolean activo;
	
	
	public FilialResponse() {
		super();
	}
	
	public FilialResponse(long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public FilialResponse(long id, String nombre, String direccion, CiudadResponse ciudad,boolean activo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.activo = activo;
	}
	public FilialResponse(String nombre, String direccion, CiudadResponse ciudad,boolean activo) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.activo = activo;
	}
	public FilialResponse(Filial filial) {
		super();
		if(filial!=null) {
			this.id = filial.getId();
			this.nombre = filial.getNombre();
			this.direccion = filial.getDireccion();
			this.ciudad = ConfiguracionMapper.toResponse(filial.getCiudad());
			this.activo = filial.getActivo();
		}
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public CiudadResponse getCiudad() {
		return ciudad;
	}
	public void setCiudad(CiudadResponse ciudad) {
		this.ciudad = ciudad;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
}
