package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Builder
@Data
@AllArgsConstructor
public class AfiliadoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String apellido;
	private String nombre;
	private String direccion;
	private String telefono;
	private String celular;
	private Long tipoDoc;
	@JsonProperty("numeroDoc")
	private String numDoc;
	private Long tipoAfiliado;
	private Long filial;
	
	
	
	public AfiliadoRequest() {
		super();
	}
	public AfiliadoRequest(Long id,String apellido, String nombre, String direccion, String telefono, String celular,
			Long tipoDoc, String numDoc, Long tipoAfiliado,Long filial) {
		super();
		this.id =id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.tipoDoc = tipoDoc;
		this.numDoc = numDoc;
		this.tipoAfiliado = tipoAfiliado;
		this.filial = filial;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Long getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Long tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNumDoc() {
		return numDoc;
	}
	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}
	public Long getTipoAfiliado() {
		return tipoAfiliado;
	}
	public void setTipoAfiliado(Long tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getFilial() {
		return filial;
	}
	public void setFilial(Long filial) {
		this.filial = filial;
	}
}
