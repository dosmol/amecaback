package com.ameca.web.presentation;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MensajeGeneralResponse {
	private String titulo;
	private ArrayList<String> mensajes;
	
	public MensajeGeneralResponse() {
		super();
	}
	public MensajeGeneralResponse(String titulo, ArrayList<String> mensajes) {
		super();
		this.titulo = titulo;
		this.mensajes = mensajes;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public ArrayList<String> getMensajes() {
		return mensajes;
	}
	public void setMensajes(ArrayList<String> mensajes) {
		this.mensajes = mensajes;
	}
	
	

}
