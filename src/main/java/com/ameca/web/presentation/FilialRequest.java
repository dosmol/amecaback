package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FilialRequest {
	private Long id;
	private String nombre,direccion;
	private CiudadRequest ciudad;
	private Boolean activo;
	public FilialRequest() {
		super();
	}
	public FilialRequest(Long id, String nombre, String direccion, CiudadRequest ciudad, Boolean activo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.activo = activo;
	}
	public FilialRequest(String nombre, String direccion, CiudadRequest ciudad, Boolean activo) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.activo = activo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public CiudadRequest getCiudad() {
		return ciudad;
	}
	public void setCiudad(CiudadRequest ciudad) {
		this.ciudad = ciudad;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
	
}