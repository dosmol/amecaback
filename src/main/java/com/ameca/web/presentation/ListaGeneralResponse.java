package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ListaGeneralResponse {
	private Long id;
	private String descripcion;
	private Boolean activo;
	private Integer orden =0;
	public ListaGeneralResponse() {
		super();
	}
	public ListaGeneralResponse(Long id, String descripcion, Boolean activo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.activo = activo;
	}
	public ListaGeneralResponse(Long id, String descripcion,Integer orden, Boolean activo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.activo = activo;
		this.orden = orden;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	
	
}
