package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import com.ameca.domain.Filial;
import com.ameca.domain.Role;
import com.ameca.domain.User;
import com.ameca.web.presentation.list.FilialResponseList;
import com.ameca.web.presentation.list.RolResponseList;

@Data
@AllArgsConstructor
public class UserResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;
	private FilialResponseList filiales;
	private RolResponseList roles;
	private LocalDateTime fechaEliminacion;
	private String rolesString="",filialesString="";
	private String email;
	private Boolean isPassReseted=true;
	
	
	
	public UserResponse(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public UserResponse(Long id,String name, Set<Filial> filiales, LocalDateTime fechaEliminacion) {
		super();
		this.id = id;
		this.name = name;
		for (Filial filial : filiales) {
			this.filiales.add((new FilialResponse(filial)));
		}
		this.fechaEliminacion = fechaEliminacion;
	}
	public UserResponse(User user) {
		super();
		this.id = user.getId();
		this.name = user.getName();
		this.filiales = new FilialResponseList();
		this.email=user.getEmail();
		if(user.getFiliales()!=null) {
			for (Filial filial : user.getFiliales()) {
				this.filiales.add((new FilialResponse(filial)));
				this.filialesString += this.filialesString.isEmpty()?filial.getNombre():", "+filial.getNombre();
			}
		}
		this.roles = new RolResponseList();
		if(user.getRoles()!=null) {
			for (Role rol : user.getRoles()) {
				this.roles.add(new RolResponse(rol));
				this.rolesString += this.rolesString.isEmpty()?rol.getDescription():", "+rol.getDescription();
			}
		}
		this.isPassReseted = user.getIsPassReseted();
		this.fechaEliminacion = user.getFechaEliminacion();
	}

	public UserResponse() {
		super();
	}

	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public FilialResponseList getFiliales() {
		return filiales;
	}
	public void setFiliales(FilialResponseList filiales) {
		this.filiales = filiales;
	}
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}
	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	public RolResponseList getRoles() {
		return roles;
	}

	public void setRoles(RolResponseList roles) {
		this.roles = roles;
	}

	public String getRolesString() {
		return rolesString;
	}

	public void setRolesString(String rolesString) {
		this.rolesString = rolesString;
	}

	public String getFilialesString() {
		return filialesString;
	}

	public void setFilialesString(String filialesString) {
		this.filialesString = filialesString;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsPassReseted() {
		return isPassReseted;
	}

	public void setIsPassReseted(Boolean isPassReseted) {
		this.isPassReseted = isPassReseted;
	}
	
	
}
