package com.ameca.web.presentation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Builder
@Data
@AllArgsConstructor
public class AfiliadoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String apellido;
	private String nombre;
	private String direccion;
	private String telefono;
	private String celular;
	private TipoDocumentoResponse tipoDoc;
	private String numDoc;
	private TipoAfiliadoResponse tipoAfiliado;
	private LocalDateTime fechaEliminacion;
	private FilialResponse filial;
	
	
	
	public AfiliadoResponse() {
		super();
	}
	public AfiliadoResponse(Long id,String apellido, String nombre, String direccion, String telefono, String celular,
			TipoDocumentoResponse tipoDoc, String numDoc, TipoAfiliadoResponse tipoAfiliado,LocalDateTime fechaEliminacion,
			FilialResponse filial) {
		super();
		this.id = id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.tipoDoc = tipoDoc;
		this.numDoc = numDoc;
		this.tipoAfiliado = tipoAfiliado;
		this.fechaEliminacion = fechaEliminacion;
		this.filial = filial;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public TipoDocumentoResponse getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(TipoDocumentoResponse tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNumDoc() {
		return numDoc;
	}
	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}
	public TipoAfiliadoResponse getTipoAfiliado() {
		return tipoAfiliado;
	}
	public void setTipoAfiliado(TipoAfiliadoResponse tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}
	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public FilialResponse getFilial() {
		return filial;
	}
	public void setFilial(FilialResponse filial) {
		this.filial = filial;
	}
	
	
	
}
