package com.ameca.web.presentation;

import com.ameca.domain.TipoAfiliado;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TipoAfiliadoResponse {
	private long id;
	private String nombre;
	private Integer orden;
	private Boolean activo;
	
	public TipoAfiliadoResponse(long id, String nombre, Integer orden) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.orden = orden;
	}
	public TipoAfiliadoResponse(TipoAfiliado tipoAfiliado) {
		super();
		this.id = tipoAfiliado.getId();
		this.nombre = tipoAfiliado.getNombre();
		this.orden = tipoAfiliado.getOrden();
		this.activo = tipoAfiliado.getActivo();
	}
	public TipoAfiliadoResponse() {
		super();
		
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
}
