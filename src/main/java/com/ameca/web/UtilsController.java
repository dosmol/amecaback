package com.ameca.web;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ameca.mapper.UtilsMapper;
import com.ameca.service.UtilsService;
import com.ameca.web.presentation.ListaGeneralResponse;
import com.ameca.domain.list.RoleList;
import com.ameca.domain.list.TipoAfiliadoList;
import com.ameca.domain.list.TipoDocumentoList;

@RestController
@RequestMapping("/utils")
@CrossOrigin
public class UtilsController extends BaseController{
	
	//Creo todos los servicios que voy a usar en el controlador
	@Autowired
	private UtilsService utilsService;
	
	
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/getTiposDocumento")
	public ResponseEntity<ArrayList<ListaGeneralResponse>> getTiposDocumento() {
		
		
		TipoDocumentoList tipos = this.utilsService.getTiposDocumento();
		

		ArrayList<ListaGeneralResponse> userResponse = UtilsMapper.toTipoDocumentoResponse(tipos);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/getTiposAfiliado")
	public ResponseEntity<ArrayList<ListaGeneralResponse>> getTiposAfiliado() {
		
		
		TipoAfiliadoList tipos = this.utilsService.getTiposAfiliado();
		

		ArrayList<ListaGeneralResponse> userResponse = UtilsMapper.toTipoAfiliadoResponse(tipos);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/getRoles")
	public ResponseEntity<ArrayList<ListaGeneralResponse>> getRoles() {
		
		
		RoleList tipos = this.utilsService.getRoles();
		

		ArrayList<ListaGeneralResponse> userResponse = UtilsMapper.toResponse(tipos);
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}

}
