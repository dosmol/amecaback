package com.ameca.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ameca.domain.User;
import com.ameca.service.UserService;

public class BaseController {
	@Autowired
	private UserService userService;
	
	protected org.springframework.security.core.userdetails.User getWebUser(){
		return (org.springframework.security.core.userdetails.User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	protected User getUser() {
		return this.userService.getUser(this.getWebUser().getUsername());
	}
}
