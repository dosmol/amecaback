package com.ameca.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ameca.domain.Ciudad;
import com.ameca.domain.Filial;
import com.ameca.domain.User;
import com.ameca.mapper.ConfiguracionMapper;
import com.ameca.service.ConfiguracionService;
import com.ameca.web.presentation.CiudadRequest;
import com.ameca.web.presentation.CiudadResponse;
import com.ameca.web.presentation.FilialRequest;
import com.ameca.web.presentation.FilialResponse;
import com.ameca.web.presentation.list.CiudadResponseList;
import com.ameca.web.presentation.list.FilialResponseList;

@RestController
@RequestMapping("/config")
@CrossOrigin
public class ConfiguracionController extends BaseController {
	
	@Autowired
	private ConfiguracionService configuracionService;


	//Manejo de las cuidades del sistema
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/ciudades")
	public ResponseEntity<CiudadResponseList> getCiudades(@RequestParam(name="soloActivas",required=false) Boolean soloActivas) {
		soloActivas = soloActivas==null?false:soloActivas; //Si no mandan parámetros, por defecto busca todas.
		CiudadResponseList ciudadesResponse = ConfiguracionMapper.toResponse(configuracionService.getCiudades(soloActivas));
		return new ResponseEntity<CiudadResponseList>(ciudadesResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/ciudad")
	public ResponseEntity<CiudadResponse> saveCiudad(@RequestBody CiudadRequest ciudadRequest) {
		Ciudad ciudad;
		try {
			if(ciudadRequest.getId() == null) {
				ciudad =ConfiguracionMapper.toDomain(ciudadRequest);
			}else {
				ciudad = this.configuracionService.getCiudad(ciudadRequest.getId()).get();
				ciudad.actualizar(ciudadRequest);	
			}
			Ciudad ciudadNew = this.configuracionService.save(ciudad);
			return new ResponseEntity<CiudadResponse>(ConfiguracionMapper.toResponse(ciudadNew),HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//Manejo de las filiales
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/filiales")
	public ResponseEntity<FilialResponseList> getFiliales(@RequestParam(name="soloActivas",required=false) Boolean soloActivas) {
		soloActivas = soloActivas==null?false:soloActivas; //Si no mandan parámetros, por defecto busca todas.
		User user = this.getUser();
		
		FilialResponseList filialResponse = ConfiguracionMapper.toResponse(configuracionService.getFiliales(user,soloActivas));
		return new ResponseEntity<FilialResponseList>(filialResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/filial")
	public ResponseEntity<FilialResponse> saveFilial(@RequestBody FilialRequest filialRequest) {
		Filial filial;
		try {
			if(filialRequest.getId() == null) {
				filial =ConfiguracionMapper.toDomain(filialRequest);
			}else {
				filial = this.configuracionService.getFilial(filialRequest.getId()).get();
				filial.actualizar(filialRequest);	
			}
			filial.setCiudad(this.configuracionService.getCiudad(filialRequest.getCiudad().getId()).get());
			
			return new ResponseEntity<FilialResponse>(ConfiguracionMapper.toResponse(this.configuracionService.save(filial)),HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
