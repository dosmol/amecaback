package com.ameca.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ameca.domain.User;
import com.ameca.domain.list.UserList;

public interface UserService extends UserDetailsService {

	User getUser(long id);
	User getUser(String userName);
	UserList getUsers();

	User save(User user);
}
