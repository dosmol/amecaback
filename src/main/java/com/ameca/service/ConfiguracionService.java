package com.ameca.service;

import java.util.HashMap;
import java.util.Optional;

import com.ameca.domain.AfiliadoFilial;
import com.ameca.domain.Ciudad;
import com.ameca.domain.Filial;
import com.ameca.domain.User;
import com.ameca.domain.list.CiudadList;
import com.ameca.domain.list.FilialList;
import com.ameca.domain.list.OperacionList;
import com.ameca.domain.list.TipoOperacionList;
public interface ConfiguracionService {
	
	
	public TipoOperacionList getTiposOperaciones();
	
	public HashMap<String, OperacionList> getTiposOperaciones(String userName);
	
	//Servicios de ciudades
	public Ciudad save(Ciudad ciudad);
	public CiudadList getCiudades(Boolean soloActivas);
	public Optional<Ciudad> getCiudad(Long id);
	
	//Servicios de filiales
	public Filial save(Filial filial);
	public FilialList getFiliales(User user,Boolean soloActivas);
	public Optional<Filial> getFilial(Long id);
	public AfiliadoFilial save(AfiliadoFilial afiFilial);

}
