package com.ameca.service.impl;

import com.ameca.domain.User;
import com.ameca.domain.list.UserList;
import com.ameca.mapper.UserDetailsMapper;
import com.ameca.repository.UserRepository;
import com.ameca.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		final User retrievedUser = userRepository.findByName(userName);
		if (retrievedUser == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}

		return UserDetailsMapper.build(retrievedUser);
	}

	@Override
	public User getUser(long id) {
		return userRepository.findById(id);
	}
	
	@Override
	public User getUser(String userName) {
		return userRepository.findByName(userName)
;	}
	
	@Override
	public UserList getUsers() {
		return userRepository.findAll();
	}

	@Override
	public User save(User user) {
		try {
			return userRepository.save(user);
		}catch (Exception e) {
			return null;
		}
		
	}

	
}
