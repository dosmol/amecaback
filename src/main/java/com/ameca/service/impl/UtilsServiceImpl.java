package com.ameca.service.impl;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ameca.domain.AfiliadoTipoAfiliado;
import com.ameca.domain.Role;
import com.ameca.domain.TipoAfiliado;
import com.ameca.domain.TipoDocumento;
import com.ameca.domain.list.RoleList;
import com.ameca.domain.list.TipoAfiliadoList;
import com.ameca.domain.list.TipoDocumentoList;
import com.ameca.repository.AfiliadoTipoAfiliadoRepository;
import com.ameca.repository.RoleRepository;
import com.ameca.repository.TipoAfiliadoRepository;
import com.ameca.repository.TipoDocumentoRepository;
import com.ameca.service.UtilsService;

@Service("utilService")
public class UtilsServiceImpl implements UtilsService{
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	@Autowired
	private TipoAfiliadoRepository tipoAfiliadoRepository;
	@Autowired
	private AfiliadoTipoAfiliadoRepository afiliadoTipoAfiliadoRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	public UtilsServiceImpl() {
		super();
	}

	@Override
	public TipoDocumentoList getTiposDocumento() {
		
		return this.tipoDocumentoRepository.findActivos();
	}
	@Override
	public Optional<TipoDocumento> getTipoDocumento(Long id) {
		
		return this.tipoDocumentoRepository.findById(id);
	}
	@Override
	public TipoAfiliadoList getTiposAfiliado() {
		
		return this.tipoAfiliadoRepository.findActivos();
	}
	@Override
	public Optional<TipoAfiliado> getTipoAfiliado(Long id) {
		
		return this.tipoAfiliadoRepository.findById(id);
	}
	@Override
	public AfiliadoTipoAfiliado saveAfiliadoTipoAfiliado(AfiliadoTipoAfiliado afiTipo) {
		return this.afiliadoTipoAfiliadoRepository.save(afiTipo);
	}
	
	@Override
	public RoleList getRoles() {
		return this.roleRepository.findAll();
	}
	
	@Override
	public Optional<Role> getRol(Long id) {
		return this.roleRepository.findById(id);
	}

}
