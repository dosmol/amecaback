package com.ameca.service.impl;

import java.util.LinkedHashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ameca.domain.AfiliadoFilial;
import com.ameca.domain.Ciudad;
import com.ameca.domain.Filial;
import com.ameca.domain.Operacion;
import com.ameca.domain.User;
import com.ameca.domain.list.CiudadList;
import com.ameca.domain.list.FilialList;
import com.ameca.domain.list.OperacionList;
import com.ameca.domain.list.TipoOperacionList;
import com.ameca.repository.AfiliadoFilialRepository;
import com.ameca.repository.CiudadRepository;
import com.ameca.repository.FilialRepository;
import com.ameca.repository.OperacionRepository;
import com.ameca.repository.TipoOperacionRepository;
import com.ameca.repository.UserRepository;
import com.ameca.service.ConfiguracionService;

@Service("configuracionService")
public class ConfiguracionServiceImpl implements ConfiguracionService{
	
	private TipoOperacionRepository tipoOPeracionRepository;
	private UserRepository userRepository;
	private OperacionRepository operacionRepository;
	private CiudadRepository ciudadRepository;
	private FilialRepository filialRepository;
	private AfiliadoFilialRepository afiliadoFilialRepository;
	
	@Autowired
	public ConfiguracionServiceImpl(TipoOperacionRepository tipoOperacionRepository,UserRepository userRepository,OperacionRepository operacionRepository,
			CiudadRepository ciudadRepository,FilialRepository filialReposotory,AfiliadoFilialRepository afiliadoFilialRepository) {
		this.tipoOPeracionRepository = tipoOperacionRepository;
		this.userRepository = userRepository;
		this.operacionRepository = operacionRepository;
		this.ciudadRepository = ciudadRepository;
		this.filialRepository = filialReposotory;
		this.afiliadoFilialRepository = afiliadoFilialRepository;
	}

	@Override
	public TipoOperacionList getTiposOperaciones() {
		return tipoOPeracionRepository.findAll();
	}
	@Override
	public LinkedHashMap<String, OperacionList> getTiposOperaciones(String userName) {
		final User usuario = userRepository.findByName(userName);
		OperacionList operaciones = this.operacionRepository.findAllByRoll(usuario.getRoles());
		LinkedHashMap<String, OperacionList> tipos = new LinkedHashMap<String, OperacionList>();
		for (Operacion operacion : operaciones) {
			if(tipos.containsKey(operacion.getTipo().getNombre())) {
				tipos.get(operacion.getTipo().getNombre()).add(operacion);
			}else {
				OperacionList opList = new OperacionList();
				opList.add(operacion);
				tipos.put(operacion.getTipo().getNombre(),opList);
			}
		}
		return tipos;
	}
	
	
	@Override
	public Ciudad save(Ciudad ciudad) {
		return this.ciudadRepository.save(ciudad);
	}
	@Override
	public CiudadList getCiudades(Boolean soloActivas) {
		if(soloActivas) {
			return this.ciudadRepository.findActivas();
		}else {
			return this.ciudadRepository.findAll();	
		}
		
	}
	@Override
	public Optional<Ciudad> getCiudad(Long id){
		return this.ciudadRepository.findById(id);
	}

	@Override
	public Filial save(Filial filial) {
		return this.filialRepository.save(filial);
	}

	@Override
	public FilialList getFiliales(User user,Boolean soloActivas) {
		if(soloActivas) {
			if(user.isAdmin()){
				return this.filialRepository.findActivas();
			}else {
				return this.filialRepository.findActivas(user.getFiliales());
			}	
		}else {
			if(user.isAdmin()){
				return this.filialRepository.findAll();	
			}else {
				return this.filialRepository.findAll(user.getFiliales());
			}
			
		}
		
	}

	@Override
	public Optional<Filial> getFilial(Long id) {
		return this.filialRepository.findById(id);
	}
	
	@Override
	public AfiliadoFilial save(AfiliadoFilial afiFilial) {
		return this.afiliadoFilialRepository.save(afiFilial);
	}

}
