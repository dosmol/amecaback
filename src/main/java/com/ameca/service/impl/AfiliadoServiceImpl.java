package com.ameca.service.impl;

import com.ameca.domain.Afiliado;
import com.ameca.domain.Filial;
import com.ameca.repository.AfiliadoRepository;
import com.ameca.service.AfiliadoService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("afiliadoService")
public class AfiliadoServiceImpl implements AfiliadoService {
	
	private AfiliadoRepository afiliadoRepository;
	
	@Autowired
	public AfiliadoServiceImpl(AfiliadoRepository afiliadoRepository) {
		this.afiliadoRepository = afiliadoRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Afiliado> findById(long id) {
		return this.afiliadoRepository.findById(id);
	}
	@Override
	public Optional<Afiliado> findBytipoDocAndnumDoc(Long tipoDoc,String numDoc) {
		return this.afiliadoRepository.findBytipoDocAndNumDoc(tipoDoc,numDoc);
	}

	@Override
	public ArrayList<Afiliado> getAfiliadosActivos() {
		return this.afiliadoRepository.getAfiliadosActivos();
	}
	
	@Override
	public ArrayList<Afiliado> getAfiliadosActivos( Set<Filial> filiales){
		return this.afiliadoRepository.getAfiliadosActivos(filiales);
	}

	@Override
	public Afiliado save(Afiliado afiliado) {
		return this.afiliadoRepository.save(afiliado);
	}
	
	@Override
	public Boolean delete(Long id) {
		Optional<Afiliado> afi = this.afiliadoRepository.findById(id);
		if(afi.isPresent()) {
			afi.get().setFechaEliminacion(LocalDateTime.now());
			this.afiliadoRepository.save(afi.get());
			return true;
		}else {
			return false;
		}
	}

	

	
}
