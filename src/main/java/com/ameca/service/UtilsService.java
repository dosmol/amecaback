package com.ameca.service;

import java.util.Optional;
import com.ameca.domain.AfiliadoTipoAfiliado;
import com.ameca.domain.Role;
import com.ameca.domain.TipoAfiliado;
import com.ameca.domain.TipoDocumento;
import com.ameca.domain.list.RoleList;
import com.ameca.domain.list.TipoAfiliadoList;
import com.ameca.domain.list.TipoDocumentoList;

public interface UtilsService {
	public TipoDocumentoList getTiposDocumento();
	public Optional<TipoDocumento> getTipoDocumento(Long id);
	public TipoAfiliadoList getTiposAfiliado();
	public Optional<TipoAfiliado> getTipoAfiliado(Long id);
	public AfiliadoTipoAfiliado saveAfiliadoTipoAfiliado(AfiliadoTipoAfiliado afiTipo);
	public RoleList getRoles();
	public Optional<Role> getRol(Long id);

}
