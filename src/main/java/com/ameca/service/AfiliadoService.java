package com.ameca.service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.ameca.domain.Afiliado;
import com.ameca.domain.Filial;


public interface AfiliadoService extends UserDetailsService {

	Optional<Afiliado>  findById(long id);
	
	ArrayList<Afiliado> getAfiliadosActivos();
	ArrayList<Afiliado> getAfiliadosActivos( Set<Filial> filiales);
	
	Optional<Afiliado> findBytipoDocAndnumDoc(Long tipoDoc,String numDoc);

	Afiliado save(Afiliado afiliado);
	
	Boolean delete(Long id);
}
