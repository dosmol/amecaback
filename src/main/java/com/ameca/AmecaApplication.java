package com.ameca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmecaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmecaApplication.class, args);
	}

}
