package com.ameca.mapper;

import java.util.ArrayList;

import com.ameca.domain.TipoDocumento;
import com.ameca.domain.Role;
import com.ameca.domain.TipoAfiliado;
import com.ameca.domain.list.RoleList;
import com.ameca.domain.list.TipoAfiliadoList;
import com.ameca.domain.list.TipoDocumentoList;
import com.ameca.web.presentation.ListaGeneralResponse;

public class UtilsMapper {
	
	public static ArrayList<ListaGeneralResponse> toTipoDocumentoResponse(TipoDocumentoList tipos) {
		ArrayList<ListaGeneralResponse> finalTipos=new ArrayList<>();
		for (TipoDocumento tipo : tipos) {
			finalTipos.add(new ListaGeneralResponse(tipo.getId(), tipo.getNombre(), tipo.getActivo()));
		}
		return finalTipos;
	} 
	public static ArrayList<ListaGeneralResponse> toTipoAfiliadoResponse(TipoAfiliadoList tipos) {
		ArrayList<ListaGeneralResponse> finalTipos=new ArrayList<>();
		for (TipoAfiliado tipo : tipos) {
			finalTipos.add(new ListaGeneralResponse(tipo.getId(), tipo.getNombre(),tipo.getOrden(), tipo.getActivo()));
		}
		return finalTipos;
	} 
	public static ArrayList<ListaGeneralResponse> toResponse(RoleList roles) {
		ArrayList<ListaGeneralResponse> finalRoles= new ArrayList<>();
		
		for (Role rol : roles) {
			finalRoles.add(new ListaGeneralResponse(rol.getId(),rol.getDescription(),true));
		}
		return finalRoles;
	} 

}
