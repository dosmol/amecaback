package com.ameca.mapper;

import java.util.ArrayList;
import com.ameca.domain.User;
import com.ameca.domain.list.UserList;
import com.ameca.web.presentation.AuthorizationRequest;
import com.ameca.web.presentation.UserResponse;

public class UserMapper {

	private UserMapper() {
	}

	public static UserResponse toResponse(User user) {
		return new UserResponse(user);	
	}
	public static ArrayList<UserResponse> toResponse(UserList users) {
		ArrayList<UserResponse> finalUsers=new ArrayList<>();
		for (User user : users) {
			finalUsers.add(new UserResponse(user));
		}
		return finalUsers;
	}

	public static User toDomain(AuthorizationRequest authorizationRequest) {
		return new User(authorizationRequest.getUser(),authorizationRequest.getEmail());
	}
}
