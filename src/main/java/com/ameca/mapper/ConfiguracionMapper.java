package com.ameca.mapper;

import java.util.HashMap;
import java.util.List;

import com.ameca.domain.Ciudad;
import com.ameca.domain.Filial;
import com.ameca.domain.Operacion;
import com.ameca.domain.TipoOperacion;
import com.ameca.domain.list.CiudadList;
import com.ameca.domain.list.FilialList;
import com.ameca.domain.list.OperacionList;
import com.ameca.domain.list.TipoOperacionList;
import com.ameca.web.presentation.CiudadRequest;
import com.ameca.web.presentation.CiudadResponse;
import com.ameca.web.presentation.FilialRequest;
import com.ameca.web.presentation.FilialResponse;
import com.ameca.web.presentation.OperacionResponse;
import com.ameca.web.presentation.TipoOperacionResponse;
import com.ameca.web.presentation.list.CiudadResponseList;
import com.ameca.web.presentation.list.FilialResponseList;
import com.ameca.web.presentation.list.OperacionResponseList;
import com.ameca.web.presentation.list.TipoOperacionResponoseList;


public class ConfiguracionMapper {
	public static TipoOperacionResponse toResponse(TipoOperacion tipo) {
		return new TipoOperacionResponse(tipo.getId(),tipo.getNombre(),tipo.getOrden(),ConfiguracionMapper.toShortResponse(tipo.getOperaciones()));
		
	}
	public static TipoOperacionResponoseList toResponse(TipoOperacionList tipos) {
		TipoOperacionResponoseList rta = new TipoOperacionResponoseList();
		for (TipoOperacion tipoOperacion : tipos) {
			 TipoOperacionResponse tipo = new TipoOperacionResponse(tipoOperacion.getId(),tipoOperacion.getNombre(),tipoOperacion.getOrden(),ConfiguracionMapper.toShortResponse(tipoOperacion.getOperaciones()));
			 rta.add(tipo);
		}
		return rta;
		
	}
	public static TipoOperacionResponoseList toResponse(HashMap<String, OperacionList> tipos) {
		TipoOperacionResponoseList rta = new TipoOperacionResponoseList();
		tipos.forEach((k,v)->{
			TipoOperacionResponse tipo = new TipoOperacionResponse(k,ConfiguracionMapper.toShortResponse(v));
			rta.add(tipo);
		});
		return rta;
		
	}
	
	public static OperacionResponseList toShortResponse(List<Operacion> operaciones) {
		OperacionResponseList rta = new OperacionResponseList();
		operaciones.forEach(operacion->{
			OperacionResponse operacionResponse = new OperacionResponse(operacion.getId(), operacion.getNombre(), operacion.getDescripcion(), operacion.getOrden(), operacion.getActivo(),operacion.getUrlComponent());
			 rta.add(operacionResponse);
		});
		
		return rta;
		
	}
	public static OperacionResponse toShortResponse(Operacion operacion) {
		return new OperacionResponse(operacion.getId(), operacion.getNombre(), operacion.getDescripcion(), operacion.getOrden(), operacion.getActivo(),operacion.getUrlComponent());
		
	}
	public static Ciudad toDomain(CiudadRequest ciudad) {
		return new Ciudad(ciudad.getNombre(),ciudad.getActivo());
	}
	public static CiudadResponse toResponse(Ciudad ciudad) {
		if(ciudad != null) {
			return new CiudadResponse(ciudad.getId(),ciudad.getNombre(),ciudad.getActivo());
		}else {
			return new CiudadResponse();
		}
		
	}
	public static CiudadResponseList toResponse(CiudadList ciudades) {
		CiudadResponseList ciudadesResponse =  new CiudadResponseList();
		ciudades.forEach(ciudad->{
			ciudadesResponse.add(new CiudadResponse(ciudad.getId(),ciudad.getNombre(),ciudad.getActivo()));
		});
		return ciudadesResponse;
	}
	
	public static FilialResponseList toResponse(FilialList filiales) {
		FilialResponseList filialesResponse =  new FilialResponseList();
		filiales.forEach(filial->{
			filialesResponse.add(new FilialResponse(filial));
		});
		return filialesResponse;
	}
	public static FilialResponse toResponse(Filial filial) {
		return new FilialResponse(filial);
	}
	public static Filial toDomain(FilialRequest filial) {
		return new Filial(filial.getId(),filial.getNombre(),filial.getDireccion(),filial.getActivo());
	}
}
