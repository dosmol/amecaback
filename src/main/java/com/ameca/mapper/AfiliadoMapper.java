package com.ameca.mapper;

import java.util.ArrayList;

import com.ameca.domain.Afiliado;
import com.ameca.web.presentation.AfiliadoRequest;
import com.ameca.web.presentation.AfiliadoResponse;
import com.ameca.web.presentation.FilialResponse;
import com.ameca.web.presentation.TipoAfiliadoResponse;
import com.ameca.web.presentation.TipoDocumentoResponse;

public class AfiliadoMapper {
	
	

	private AfiliadoMapper() {
	}

	public static AfiliadoResponse toResponse(Afiliado afiliado) {
		AfiliadoResponse afi = new AfiliadoResponse(afiliado.getId(),afiliado.getApellido(),afiliado.getNombre(),afiliado.getDireccion(),afiliado.getTelefono(),afiliado.getCelular(),
				new TipoDocumentoResponse( afiliado.getTipoDoc()),afiliado.getNumDoc(),
				new TipoAfiliadoResponse(afiliado.getTipoActual()),afiliado.getFechaEliminacion(),
				new FilialResponse(afiliado.getFilialActual()));
		return afi;
		
	}
	public static ArrayList<AfiliadoResponse> toResponse(ArrayList<Afiliado> afiliados) {
		ArrayList<AfiliadoResponse> finalAfiliados=new ArrayList<>();
		for (Afiliado afiliado : afiliados) {
			finalAfiliados.add(new AfiliadoResponse(afiliado.getId(),afiliado.getApellido(),afiliado.getNombre(),afiliado.getDireccion(),afiliado.getTelefono(),afiliado.getCelular(),
					new TipoDocumentoResponse( afiliado.getTipoDoc()),afiliado.getNumDoc(),
					new TipoAfiliadoResponse(afiliado.getTipoActual()),afiliado.getFechaEliminacion(),
					new FilialResponse(afiliado.getFilialActual())));
		}
		return finalAfiliados;
	}

	public static Afiliado toDomain(AfiliadoRequest ar) {
		return new Afiliado(ar.getNombre(),ar.getApellido(),ar.getDireccion(),ar.getTelefono(),ar.getCelular(),ar.getNumDoc());
	}
}
