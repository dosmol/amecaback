package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.ameca.domain.AfiliadoTipoAfiliado;

@Component
public interface AfiliadoTipoAfiliadoRepository extends JpaRepository<AfiliadoTipoAfiliado, Long>{
}
