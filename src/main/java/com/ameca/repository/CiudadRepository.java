package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import com.ameca.domain.Ciudad;
import com.ameca.domain.list.CiudadList;

@Component
public interface CiudadRepository extends JpaRepository<Ciudad, Long>{
	
	public CiudadList findAll();
	
	@Query("SELECT c FROM Ciudad c WHERE  c.activo = true")
	public CiudadList findActivas();
}
