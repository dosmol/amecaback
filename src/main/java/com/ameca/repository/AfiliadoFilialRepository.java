package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import com.ameca.domain.AfiliadoFilial;

@Component
public interface AfiliadoFilialRepository extends JpaRepository<AfiliadoFilial, Long>{
}
