package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.ameca.domain.TipoAfiliado;
import com.ameca.domain.list.TipoAfiliadoList;

@Component
public interface TipoAfiliadoRepository extends JpaRepository<TipoAfiliado, Long>{
	@Query("SELECT ta FROM TipoAfiliado ta WHERE ta.activo = 1 ORDER BY ta.orden asc")
	TipoAfiliadoList findActivos();
}
