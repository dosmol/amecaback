package com.ameca.repository;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import com.ameca.domain.Filial;
import com.ameca.domain.list.FilialList;

@Component
public interface FilialRepository extends JpaRepository<Filial, Long>{
	
	public FilialList findAll();
	
	@Query("SELECT f FROM Filial f WHERE f IN (:filiales)")
	public FilialList findAll(@Param("filiales")Set<Filial> set);
	
	@Query("SELECT f FROM Filial f WHERE  f.activo = true")
	public FilialList findActivas();
	
	@Query("SELECT f FROM Filial f WHERE  f.activo = true AND f IN (:filiales)")
	public FilialList findActivas(@Param("filiales")Set<Filial> set);
}
