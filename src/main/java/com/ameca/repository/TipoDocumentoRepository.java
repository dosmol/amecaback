package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.ameca.domain.TipoDocumento;
import com.ameca.domain.list.TipoDocumentoList;

@Component
public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Long>{
	@Query("SELECT td FROM TipoDocumento td WHERE td.activo = 1")
	TipoDocumentoList findActivos();

}
