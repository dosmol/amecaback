package com.ameca.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.ameca.domain.Role;
import com.ameca.domain.TipoOperacion;
import com.ameca.domain.list.TipoOperacionList;

@Component
public interface TipoOperacionRepository extends JpaRepository<TipoOperacion, Long>{
	
	public TipoOperacionList findAll();
	
	@Query("SELECT top FROM Operacion op inner join op.tipo top inner join op.roles rol WHERE top.activo = 1 AND rol IN (:idRol) GROUP BY top")
	public TipoOperacionList findAllByRoll(@Param("idRol") Set<Role> roles);

}
