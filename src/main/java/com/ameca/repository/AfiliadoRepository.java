package com.ameca.repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import com.ameca.domain.Afiliado;
import com.ameca.domain.Filial;


@Component
public interface AfiliadoRepository extends JpaRepository<Afiliado, Long>{
	Optional<Afiliado> findById(Long id);
	
	@Query("SELECT a FROM Afiliado a WHERE a.fechaEliminacion = null")
	ArrayList<Afiliado> getAfiliadosActivos();
	
	@Query("SELECT a FROM Afiliado a INNER JOIN a.filiales af INNER JOIN af.filial f WHERE a.fechaEliminacion = null and f IN (:filiales) AND af.fechaBaja IS NULL")
	ArrayList<Afiliado> getAfiliadosActivos(@Param("filiales") Set<Filial> filiales);
	
	@Query("SELECT a FROM Afiliado a WHERE  a.numDoc = :numDoc AND a.tipoDoc.id = :tipoDoc")
	Optional<Afiliado> findBytipoDocAndNumDoc(@Param("tipoDoc")Long tipoDoc, @Param("numDoc")String numDoc);
}
