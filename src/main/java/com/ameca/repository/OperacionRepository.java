package com.ameca.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import com.ameca.domain.Operacion;
import com.ameca.domain.Role;
import com.ameca.domain.list.OperacionList;

@Component
public interface OperacionRepository extends JpaRepository<Operacion, Long>{
	
	public OperacionList findAll();
	
	//@Query("SELECT top FROM TipoOperacion top right join top.operaciones op inner join op.roles rol WHERE top.activo = 1 AND rol IN (:idRol)")
	@Query("SELECT op FROM Operacion op inner join op.tipo top inner join op.roles rol WHERE top.activo = 1 AND rol IN (:idRol) ORDER BY top.orden,op.orden")
	public OperacionList findAllByRoll(@Param("idRol") Set<Role> roles);

}
