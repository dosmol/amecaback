package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import com.ameca.domain.Role;
import com.ameca.domain.list.RoleList;

@Component
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	public RoleList findAll();

	Role getRoleById(long id);

	Role findByName(String name);
}
