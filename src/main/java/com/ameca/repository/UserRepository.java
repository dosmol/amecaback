package com.ameca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import com.ameca.domain.User;
import com.ameca.domain.list.UserList;

@Component
public interface UserRepository extends JpaRepository<User, Long> {

	public UserList findAll();
	
	@Query("SELECT u FROM User u WHERE u.name = :name AND u.fechaEliminacion = NULL")
	public User findByName(@Param("name") String  name);

	public User findById(long id);

}
